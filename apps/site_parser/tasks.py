from bs4 import BeautifulSoup
from celery.schedules import crontab
from celery.task import periodic_task
from requests_xml import XMLSession
from rest_framework import status
from rest_framework.response import Response

from apps.site_parser.models import Article


@periodic_task(run_every=(crontab(minute='*/10')))
def prase_hacker_news(template=None):
    site_url = 'https://news.ycombinator.com/'
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 '
                             '(KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    session = XMLSession()
    result = session.get(site_url, headers=headers)
    if result.status_code != 200:
        return Response({'error': "HTTP site error status {}".format(result.status_code)},
                        status=status.HTTP_400_BAD_REQUEST)
    text = result.xml.xml
    soup = BeautifulSoup(text, 'xml')
    if template:
        soup = BeautifulSoup(open(template))
    objects = soup.findAll("tr", {"class": "athing"})
    for item in objects:
        news_item = item.find("a", {"class": "storylink"})
        if Article.objects.filter(url=news_item.attrs['href']).exists():
            continue
        elif len(Article.objects.all()) > 30:
            first_post = Article.objects.first()
            first_post.title = news_item.text
            first_post.url = news_item.attrs['href']
            first_post.save()
        else:
            Article.objects.create(title=news_item.text, url=news_item.attrs['href'])

    return Response({}, status=status.HTTP_200_OK)
