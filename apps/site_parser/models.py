from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=256, verbose_name="title")
    url = models.URLField(verbose_name="article url")
    created_at = models.DateTimeField(auto_now=True, verbose_name="creation timestamp")

    class Meta:
        verbose_name = "article"
        verbose_name_plural = "articles"
        ordering = ['created_at']

    def __str__(self):
        return '%s' % (self.title,)