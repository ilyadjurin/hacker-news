from django.contrib import admin

from apps.site_parser.models import Article

admin.site.register(Article)
