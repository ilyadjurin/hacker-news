from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIRequestFactory

from apps.site_parser.api.views import ArticleListView
from apps.site_parser.models import Article
from apps.site_parser.tasks import prase_hacker_news


class TestSiteParserApi(APITestCase):

    def setUp(self):
        super().setUp()

    def test_site_parser(self):
        factory = APIRequestFactory()

        prase_hacker_news('./apps/site_parser/test_templates/Hacker News.html')
        self.assertEqual(len(Article.objects.all()), 30)

        url = reverse('posts')
        response = self.client.get(url)
        self.assertEqual(response.json()['count'], 30)

        request = factory.get('/posts/?limit=10')
        view = ArticleListView.as_view()
        response = view(request)
        self.assertEqual(len(response.data['results']), 10)
