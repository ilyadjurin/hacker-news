from rest_framework import filters
from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.site_parser.models import Article
from apps.site_parser.tasks import prase_hacker_news
from .serializers import ArticleListSerializer


class ArticleListView(ListAPIView):
    """
    Article list
    """

    permission_classes = (AllowAny,)
    queryset = Article.objects.all()
    serializer_class = ArticleListSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('id', 'title', 'created_at')


class NewsGrubber(APIView):
    """
    Api for force execution parse Hacker News
    """

    def get(self, request):
        try:
            prase_hacker_news()
        except Exception:
            return Response({'error': 'error parse Hacker News resource'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'status': 'successful parse Hacker News resource'}, status=status.HTTP_200_OK)
