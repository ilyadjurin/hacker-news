VENV=./hackvenv

source $VENV/bin/activate

lockfile python.lock
rm -f python.lock

if [[ $1 != "beat" ]]; then
    set -m
    su -m root -c "./$VENV/bin/celery worker  -A hacker_news.celery  -l info -Q default -n default@%h"&fg %1
else
    if [[ -f celerybeat.pid ]]; then rm celerybeat.pid; fi
    if [[ -f celerybeat-schedule ]]; then rm celerybeat-schedule; fi
    ./$VENV/bin/celery  -A hacker_news.celery  -l info beat
fi