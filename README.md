# Hacker_news parser

Проект развернут на http://hackernews.autoton.biz/

Для принудительного парсинга ресура вызовите API: /api/v1/parser

Ресурс парсится раз в 10 минут

Стандартный логин/пароль для входа в админ-панель:

``
admin : testpass
``


## Развертывание проекта
```
cp .env_default .env && nano .env
cd conf
docker-compose build
docker-compose up
```
Далее внутри контейнера можно создавать суперюзера и выполнять все необходимые действия


## Тестирование
Все API проекта покрыты тестами.

Для запуска тестов используйте команду

```
python3 manage.py test apps.site_parser