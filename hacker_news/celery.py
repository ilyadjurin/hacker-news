from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hacker_news.settings')
app = Celery('hacker_news')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.enable_utc = False

app.autodiscover_tasks()
